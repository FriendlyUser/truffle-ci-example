[![Build Status](https://travis-ci.org/FriendlyUser/truffle-ci-example.svg?branch=master)](https://travis-ci.org/FriendlyUser/truffle-ci-example) [![Solidity Coverage Status](https://coveralls.io/repos/github/FriendlyUser/truffle-ci-example/badge.svg?branch=master)](https://coveralls.io/github/FriendlyUser/truffle-ci-example?branch=master)

# truffle-ci

 
A simple example how to use continuous integration with travis-ci and truffle by running a blockchain in the background.
This is a template project for when I learn blockchain development.

```yml
language: node_js
node_js:
  - "8"
script:
  - npm install -g truffle
  - npm i --save-dev run-with-testrpc
  - truffle compile
  - ./node_modules/.bin/run-with-testrpc 'truffle migrate && truffle test'
```
